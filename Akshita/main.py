#Import necessary libraries
from flask import Flask, render_template, request
import pandas as pd
import numpy as np
import spacy
from spacy import displacy
import tensorflow as tf
from tensorflow.keras.preprocessing.image import  load_img
from tensorflow.keras.preprocessing.image import img_to_array
from PIL import Image
import os
import cv2
import pytesseract

# Create flask instance
app = Flask(__name__)

#Set Max size of file as 10MB.
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

#Allow files with extension png, jpg and jpeg
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Function to load and prepare the image in right shape
def read_image(filename):
    # Load the image
    img = load_img(filename, color_mode="grayscale", target_size=(48, 48))
    # Convert the image to array
    img = img_to_array(img)
    # Reshape the image into a sample of 1 channel

    img = img.reshape(1, 48, 48, 1)
    # Prepare it as pixel data
    img = img.astype('float32')
    img = img / 255.0
    return img

@app.route("/", methods=['GET', 'POST'])
def home():

    return render_template('home.html')

@app.route("/predict", methods = ['GET','POST'])
def predict():
   if request.method == 'POST':
        file = request.files['file']
        filename = file.filename
        file_path = os.path.join('static/images', filename)
        file.save(file_path)
        img = read_image(file_path)
        path=r"Tesseract-OCR/tesseract.exe"
        Imagepath=file_path
        pytesseract.pytesseract.tesseract_cmd=path
        text=pytesseract.image_to_string(Image.open(Imagepath))
        print(text[:-1])
        NER = spacy.load("en_core_web_sm")
        text1= NER(text)
        l=[]
        for word in text1.ents:
            l.append((word.text,word.label_))
        return render_template('predict.html', product = text[:-1], user_image = file_path,product1=l)



    
if __name__ == "__main__":
    app.run(debug=True)